
import com.typesafe.sbt.SbtScalariform._
import org.scalastyle.sbt.ScalastylePlugin._
import scalariform.formatter.preferences._

val javaVersion = "1.8"
val encoding = "utf-8"
val scalaVersionString = "2.11.8"

val scalazVersion: String = "7.2.6"

val specs2Version: String = "3.8.5"

val akkaVerion: String = "2.4.11"

val testDependencies = Seq(
  "com.github.tomakehurst" % "wiremock" % "1.58",
  "org.specs2" %% "specs2-core" % specs2Version,
  "org.specs2" %% "specs2-scalacheck" % specs2Version,
  "org.specs2" %% "specs2-junit" % specs2Version,
  "org.specs2" %% "specs2-mock" % specs2Version,
  "com.typesafe.akka" %% "akka-testkit" % akkaVerion
).map(_.exclude("log4j", "log4j")).map(_.exclude("commons-logging", "commons-logging")).map(_ % "test")

val appDependencies: Seq[sbt.ModuleID] = Seq(
  "com.softwaremill.macwire" %% "macros" % "2.2.5",
  "org.slf4j" % "jcl-over-slf4j" % "1.5.11",
  "org.slf4j" % "log4j-over-slf4j" % "1.7.13",
  "com.typesafe.akka" %% "akka-actor" % akkaVerion,
  "com.typesafe.akka" %% "akka-stream" % akkaVerion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVerion,
  "com.typesafe.akka" %% "akka-http-experimental" % akkaVerion,
  "ch.qos.logback" % "logback-classic" % "1.1.7",
  "com.github.melrief" %% "pureconfig" % "0.3.3"
).map(_.exclude("log4j", "log4j")).map(_.exclude("commons-logging", "commons-logging"))

val prodScalacOptions = Seq(
  "-feature",
  "-language:postfixOps",
  "-target:jvm-" + javaVersion,
  "-unchecked",
  "-deprecation",
  "-encoding", encoding,
  "-Ywarn-dead-code",
  "-Ywarn-infer-any",
  "-Ybackend:GenBCode",
  "-Ydelambdafy:method",
  "-target:jvm-1.8")

lazy val compileScalastyle = taskKey[Unit]("compileScalastyle")


lazy val root = Project(id = "carjump", base = file("."), settings = defaultScalariformSettings).settings(
  name := "carjump-challenge",
  organization := "net.carjump",
  shellPrompt := { state => scala.Console.CYAN + "🚗  carjump  > " + scala.Console.RESET },
  scalaVersion := scalaVersionString,
  scalaVersion in ThisBuild := scalaVersionString,
  libraryDependencies ++= appDependencies ++ testDependencies,
  javacOptions ++= Seq("-source", javaVersion, "-target", javaVersion, "-Xlint"),
  scalacOptions ++= prodScalacOptions,
  scalacOptions in Test ~= { (options: Seq[String]) =>
    options filterNot (option => option.startsWith("-X") || Seq("-Ywarn-dead-code", "-Ywarn-infer-any").contains(option))
  },
  testOptions in Test += Tests.Argument("-Dlogback.statusListenerClass=ch.qos.logback.core.status.NopStatusListener"),
  scalastyleFailOnError := true,
  compileScalastyle := org.scalastyle.sbt.ScalastylePlugin.scalastyle.in(Compile).toTask("").value,
  (compile in Compile) <<= (compile in Compile) dependsOn compileScalastyle,
  parallelExecution in Test := false,
  testOptions in Test += Tests.Argument("sequential"),

  /**
    * scalariform
    */
  ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference(RewriteArrowSymbols, false)
    .setPreference(AlignArguments, true)
    .setPreference(DoubleIndentClassDeclaration, true)
    .setPreference(AlignSingleLineCaseStatements, true)
    .setPreference(SpacesAroundMultiImports, true)
    .setPreference(AlignParameters, true)
    .setPreference(RewriteArrowSymbols, true)
)
