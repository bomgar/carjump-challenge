package net.carjump

import akka.testkit.TestFSMRef
import org.specs2.mutable.Specification
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Await
import scala.concurrent.duration._

class StoringActorSpec extends Specification {
  "StoringActor" should {

    implicit val timeout = Timeout(5.seconds)

    "return nothing when empty" in new AkkaTestkitSpecs2Support() {
      val storingActor = TestFSMRef(new StoringActor())
      storingActor.stateName must be equalTo StoringActor.Empty
      val getResult = Await.result((storingActor ask StoringActor.Get).mapTo[StoringActor.GetResult], 10.seconds)
      getResult.compressedList must beNone
    }

    "return stored list" in new AkkaTestkitSpecs2Support() {
      val storingActor = TestFSMRef(new StoringActor())
      storingActor.stateName must be equalTo StoringActor.Empty
      storingActor ! StoringActor.Store(CompressedList(List()))
      storingActor.stateName must be equalTo StoringActor.HasList
      storingActor.stateData.asInstanceOf[StoringActor.CompressedData].compressedList must be equalTo CompressedList(List())

      val getResult = Await.result((storingActor ask StoringActor.Get).mapTo[StoringActor.GetResult], 10.seconds)
      getResult.compressedList must beSome
    }
  }
}
