package net.carjump

import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration._

import akka.actor.ActorSystem
import akka.stream.{ ActorMaterializer, Materializer }
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import org.specs2.mutable.After

trait WiremockScope extends After {

  lazy implicit val as: ActorSystem = ActorSystem()
  lazy implicit val ec: ExecutionContext = as.dispatcher
  lazy implicit val mat: Materializer = ActorMaterializer()

  lazy val port = 13089
  lazy val wireMockServer: WireMockServer = new WireMockServer(wireMockConfig().port(port))
  wireMockServer.start()

  override def after: Unit = {
    wireMockServer.stop()
    as.terminate()
    Await.result(as.whenTerminated.map(_ ⇒ ()), 1.seconds)
  }
}
