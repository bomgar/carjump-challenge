package net.carjump

import org.specs2.mutable.Specification
import scala.concurrent.Await
import scala.concurrent.duration._
import com.github.tomakehurst.wiremock.client.WireMock._

class CarjumpClientSpec extends Specification {
  trait WithCarjumpClient extends WiremockScope {
    import com.softwaremill.macwire._
    lazy val carjumpClientConfig = CarjumpClientConfig(s"http://localhost:$port/A")
    lazy val compressor = wire[Compressor]
    lazy val carjumpClient = wire[CarjumpClient]
  }

  "A CarjumpClient" should {

    "return an error if carjump does not respond with 200" in new WithCarjumpClient() {
      wireMockServer.stubFor(get(urlEqualTo("/A"))
        .willReturn(aResponse()
          .withStatus(400)))
      Await.result(carjumpClient.getCarjumpData, 10.seconds) must throwA(new RuntimeException("Failed to fetch data from carjump."))
    }

    "return compressed list" in new WithCarjumpClient() {
      wireMockServer.stubFor(get(urlEqualTo("/A"))
        .willReturn(aResponse()
          .withStatus(200)
          .withHeader("Content-Type", "text/plain")
          .withBody("a\na\nx\nb\nb")))
      val list = Await.result(carjumpClient.getCarjumpData, 10.seconds)
      list must be equalTo CompressedList(List(Compressed(2, "a"), Compressed(1, "x"), Compressed(2, "b")))
    }
  }
}
