package net.carjump

import akka.stream.scaladsl.Source
import org.specs2.mutable.Specification
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class CompressedListSpec extends Specification {

  def timeout = 20.seconds

  abstract class WithCompressor extends AkkaTestkitSpecs2Support {
    lazy val compressor = new Compressor
  }

  "A compressor" should {
    "compress lists" in new WithCompressor() {
      val compressedList: CompressedList[String] = Await.result(compressor.compress(Source(List("a", "a", "b", "b", "b", "b", "b", "b", "c"))), timeout)
      compressedList must be equalTo CompressedList(List(Compressed(2, "a"), Compressed(6, "b"), Compressed(1, "c")))
      compressedList.length must be equalTo 9
    }

    "compress empty lists" in new WithCompressor() {
      val compressedList: CompressedList[String] = Await.result(compressor.compress(Source(List.empty[String])), timeout)
      compressedList must be equalTo CompressedList(List.empty)
      compressedList.length must be equalTo 0
    }
  }
  "A compressed list" should {
    "get an element at index" in {
      val list = List("a", "a", "b", "b", "b", "b", "b", "b", "c")
      val compressedList = CompressedList(List(Compressed(2, "a"), Compressed(6, "b"), Compressed(1, "c")))
      list.zipWithIndex.foreach {
        case (elem, index) ⇒
          compressedList.get(index.toLong) must beSome(elem)
      }
      compressedList.get(-1) must beNone
      compressedList.get(list.length.toLong) must beNone
      compressedList.length must be equalTo 9
    }
  }
}
