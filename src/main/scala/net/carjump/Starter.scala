package net.carjump

import scala.concurrent.{ ExecutionContext, Future }

import akka.http.scaladsl.Http.ServerBinding
import pureconfig._
import scala.util.{ Try, Success, Failure }

object Starter extends App {
  val conf: Try[CarjumpConfig] = loadConfig[CarjumpConfig]
  conf match {
    case Success(config) ⇒
      val module = new CarjumpModule() {
        override lazy val carjumpConfig = config
      }
      implicit val ec: ExecutionContext = module.executionContext

      val bindingFuture: Future[ServerBinding] = module.start
      for (ln ← io.Source.stdin.getLines) {}
      bindingFuture
        .flatMap(_.unbind())
        .onComplete(_ ⇒ module.system.terminate())
    case Failure(e) ⇒
      println("Failed to parse config: " + e.getMessage)
      System.exit(1)
  }

}

