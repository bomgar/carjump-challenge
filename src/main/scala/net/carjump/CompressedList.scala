package net.carjump

import akka.stream.scaladsl.Sink
import akka.stream.stage.{ GraphStageLogic, InHandler, OutHandler }
import akka.stream.{ Attributes, FlowShape, Inlet, Materializer, Outlet }
import akka.stream.scaladsl.Source
import akka.stream.stage.GraphStage
import scala.annotation.tailrec
import scala.collection.generic.CanBuildFrom
import scala.concurrent.{ ExecutionContext, Future }
import scala.language.higherKinds

case class Compressed[T](size: Long, value: T)

case class CompressedList[T](list: List[Compressed[T]]) {
  def length: Long = list.map(_.size).sum

  def get(index: Long): Option[T] = {

    @tailrec
    def get(list: List[Compressed[T]], index: Long): Option[T] = list match {
      case head :: tail if index < head.size ⇒ Some(head.value)
      case head :: tail                      ⇒ get(tail, index - head.size)
      case Nil                               ⇒ None
    }

    if (index < 0)
      None
    else
      get(list, index)
  }
}

class Compressor(implicit mat: Materializer, ec: ExecutionContext) {

  def compress[T](elements: Source[T, Any]): Future[CompressedList[T]] = {
    elements
      .via(new CompressingStage[T]())
      .runWith(collectingSink[Compressed[T], List])
      .map(CompressedList(_))
  }

  private def collectingSink[U, T[_]](implicit cbf: CanBuildFrom[T[U], U, T[U]]): Sink[U, Future[T[U]]] =
    Sink.fold(cbf()) { (builder, element: U) ⇒
      builder += element
    }.mapMaterializedValue(_.map(_.result()))

  private class CompressingStage[A]() extends GraphStage[FlowShape[A, Compressed[A]]] {
    val in = Inlet[A]("Compressing.in")
    val out = Outlet[Compressed[A]]("Compressing.out")
    val shape = FlowShape.of(in, out)

    override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
      new GraphStageLogic(shape) {

        var compressed: Option[Compressed[A]] = Option.empty[Compressed[A]]

        setHandler(in, new InHandler {
          override def onPush(): Unit = {
            val elem = grab(in)
            compressed match {
              case Some(c) if c.value == elem ⇒
                compressed = Some(c.copy(size = c.size + 1))
                pull(in)
              case Some(c) ⇒
                push(out, c)
                compressed = Some(Compressed(1, elem))
              case None ⇒
                compressed = Some(Compressed(1, elem))
                pull(in)
            }
          }
          override def onUpstreamFinish(): Unit = {
            if (compressed.isDefined) emit(out, compressed.get)
            complete(out)
          }
        })
        setHandler(out, new OutHandler {
          override def onPull(): Unit = {
            pull(in)
          }
        })
      }
  }

}

