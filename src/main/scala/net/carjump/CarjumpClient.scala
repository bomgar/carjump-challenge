package net.carjump

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ HttpRequest, HttpResponse, StatusCodes }
import akka.stream.Materializer
import akka.stream.scaladsl.{ Flow, Framing, Source }
import akka.util.ByteString
import scala.concurrent.{ ExecutionContext, Future }
import net.carjump.utils.SLF4JLogging

case class CarjumpClientConfig(uri: String)

class CarjumpClient(config: CarjumpClientConfig, compressor: Compressor)(implicit ec: ExecutionContext, system: ActorSystem, mat: Materializer) extends SLF4JLogging {

  val http = Http(system)

  val framingDelimiter: Flow[ByteString, ByteString, akka.NotUsed] = Framing.delimiter(
    ByteString("\n"),
    maximumFrameLength = 1,
    allowTruncation    = true
  )

  def getCarjumpData(): Future[CompressedList[String]] = {
    http.singleRequest(HttpRequest(uri = config.uri)).flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) ⇒
        log.debug("Ok response from carjump.")
        val source: Source[String, Any] = entity
          .dataBytes
          .via(framingDelimiter)
          .map(_.utf8String)
        compressor.compress[String](source)
      case resp @ HttpResponse(code, _, _, _) ⇒
        log.info("Request failed, response code: {}", code)
        resp.discardEntityBytes()
        Future.failed(new RuntimeException("Failed to fetch data from carjump."))
    }
  }
}
