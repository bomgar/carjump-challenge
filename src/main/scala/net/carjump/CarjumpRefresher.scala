package net.carjump

import akka.actor.ActorSystem
import akka.stream.scaladsl.{ Sink, Source }
import akka.stream.{ ActorMaterializer, ActorMaterializerSettings, Materializer, Supervision }
import scala.concurrent.duration._

case class CarjumpRefresherConfig(interval: FiniteDuration)

class CarjumpRefresher(carjumpClient: CarjumpClient, storingActorRef: StoringActorRef, refresherConfig: CarjumpRefresherConfig)(implicit system: ActorSystem) {
  val decider: Supervision.Decider = {
    case _ ⇒ Supervision.Resume
  }
  implicit val materializer: Materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))

  def start(): Unit = {
    Source
      .tick(1.seconds, refresherConfig.interval, ())
      .mapAsync(1)(_ ⇒ carjumpClient.getCarjumpData)
      .map(StoringActor.Store)
      .runWith(Sink.actorRef(storingActorRef.ref, ()))
  }
}
