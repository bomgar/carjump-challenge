package net.carjump

import akka.actor.Props
import scala.concurrent.Future
import scala.util.{ Failure, Success }
import com.softwaremill.macwire._
import akka.http.scaladsl.Http.ServerBinding
import net.carjump.utils.SLF4JLogging

case class BindingConfig(interface: String, port: Int)
case class CarjumpConfig(client: CarjumpClientConfig, binding: BindingConfig, refresher: CarjumpRefresherConfig)

trait CarjumpModule extends SLF4JLogging {

  import akka.actor.ActorSystem
  import akka.stream.ActorMaterializer

  implicit val system = ActorSystem("carjump-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  def carjumpConfig: CarjumpConfig

  lazy val carjumpClientConfig: CarjumpClientConfig = carjumpConfig.client

  lazy val carjumpRefresherConfig: CarjumpRefresherConfig = carjumpConfig.refresher

  lazy val storingActorRef: StoringActorRef = StoringActorRef(system.actorOf(Props(wire[StoringActor]), "StoringActor"))

  lazy val compressor: Compressor = wire[Compressor]

  lazy val carjumpClient: CarjumpClient = wire[CarjumpClient]

  lazy val server: WebServer = wire[WebServer]

  lazy val carjumpRefresher: CarjumpRefresher = wire[CarjumpRefresher]

  def start: Future[ServerBinding] = {
    carjumpRefresher.start()
    val bindingFuture = server.start(carjumpConfig.binding.interface, carjumpConfig.binding.port)
    bindingFuture.onComplete {
      case Success(b) ⇒
        log.info("Server started successfully: {}. Press C-d to terminate.", b)
      case Failure(e) ⇒
        log.error("Server failed to start", e)
        System.exit(1)
    }
    bindingFuture
  }
}
