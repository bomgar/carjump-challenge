package net.carjump

import akka.actor.{ ActorLogging, ActorRef, FSM }

import StoringActor._

case class StoringActorRef(ref: ActorRef)

class StoringActor extends FSM[StoringState, StoringData] with ActorLogging {
  startWith(Empty, NoData)

  when(Empty) {
    case Event(Get, _) ⇒
      log.info("Get request for uninitialized storage.")
      sender ! GetResult(None)
      stay
    case Event(Store(list), _) ⇒
      log.info("Storage initialized with {} elements.", list.length)
      goto(HasList) using CompressedData(list)
  }

  when(HasList) {
    case Event(Get, CompressedData(list)) ⇒
      sender ! GetResult(Some(list))
      stay
    case Event(Store(list), _) ⇒
      log.info("Storage updated with {} elements.", list.length)
      stay using CompressedData(list)
  }
}

object StoringActor {
  sealed trait StoringMessage
  case object Get
  case class Store(compressedList: CompressedList[String])

  sealed trait StoringState
  case object Empty extends StoringState
  case object HasList extends StoringState

  sealed trait StoringData
  case object NoData extends StoringData
  case class CompressedData(compressedList: CompressedList[String]) extends StoringData

  sealed trait StoringResult
  case class GetResult(compressedList: Option[CompressedList[String]]) extends StoringResult
}
